////
////  SwipingView.swift
////  DatingApp
////
////  Created by Calvin Usiri on 12/27/16.
////  Copyright © 2016 Calvin UsiriAuquem. All rights reserved.
////
//
import UIKit

class SwipingView: UIView
{
    var swipingViewAnimator: UIDynamicAnimator!
    var swipingViewOriginalCenter: CGPoint!
    let imageMarginSpace: CGFloat = 5.0
    var swipingViewImageStringFromFirebaseAsString : String!
    var swipingViewNameFromFirebaseAsString : String!
    var swipingViewLocationOfUserAsString : String!
    var swipingViewOccupationFromFirebaseAsString : String!
    var swipingViewImageViewObject : UIImageView!
    var swipingViewLabelsContainer : UIView!
    var swipingViewNameLabelObject : UILabel!
    var swipingViewLocationLabelObject : UILabel!
    var swipingViewOccupationLabelObject : UILabel!
    let swipingViewActivityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    init(frame: CGRect, center : CGPoint,imageTmp : String,name : String,location: String ,occupation : String)
    {
        super.init(frame: frame)
        self.center = center
        self.swipingViewImageStringFromFirebaseAsString = imageTmp
        self.swipingViewNameFromFirebaseAsString = name
        self.swipingViewLocationOfUserAsString = location
        self.swipingViewOccupationFromFirebaseAsString = occupation
        self.backgroundColor = UIColor.blue         // Set the background to white
        self.layer.shouldRasterize = true
        self.swipingViewOriginalCenter = center
        self.swipingViewLabelsContainer = UIView()
        self.swipingViewImageViewObject = UIImageView()
        self.swipingViewNameLabelObject = UILabel()
        self.swipingViewLocationLabelObject = UILabel()
        self.swipingViewOccupationLabelObject = UILabel()
        swipingViewAnimator = UIDynamicAnimator(referenceView: self) // Apple thing. For physics
        
        // Custom Activity Indicator
        let progressHUD = ProgressHUD(text: "Making Moves")
        self.addSubview(progressHUD)
        
        self.contentMode = .scaleAspectFill
        self.layer.cornerRadius = 20 //curvy edges
        self.layer.masksToBounds = true
        
        //Label and SwipingImage
        if let url = NSURL(string: imageTmp)
        {
            DispatchQueue.global(qos: .background).async
            {
                progressHUD.show()
                if let data = NSData(contentsOf: url as URL)
                {
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async
                    {
                        self.swipingViewImageViewObject.image = UIImage(data: data as Data)
                    }
                }else
                {
                    print("Image did not set")
                }
            }
        }else
        {
            print("\nDidnt work\n")
            progressHUD.hide()
        }
        // Move to a background thread to do some long running work
            
        self.swipingViewImageViewObject.frame = CGRect(self.bounds.width/2*0.0005,self.bounds.height/2*0.0005,self.bounds.width,self.bounds.height) //smaller x-to move left,y- to move up,W, H
        self.addSubview(swipingViewImageViewObject)
        
        self.swipingViewLabelsContainer.backgroundColor = UIColor.white
        self.swipingViewLabelsContainer.alpha = 0.5
        self.swipingViewLabelsContainer.frame = CGRect(self.bounds.width/16,self.bounds.height/1.5,self.bounds.width/1.15,self.bounds.height*0.25)
        self.swipingViewLabelsContainer.layer.cornerRadius = 10 //curvy edges
        self.swipingViewLabelsContainer.layer.masksToBounds = true
        self.addSubview(swipingViewLabelsContainer)
        
        let tmp = 26
        self.swipingViewNameLabelObject.text = "\(name), \(tmp)"
        self.swipingViewNameLabelObject.font = UIFont(name: "HelveticaNeue-Light", size: 22.0)
        self.swipingViewNameLabelObject.frame = CGRect(self.bounds.width/10,self.bounds.height/1.45,self.bounds.width/1.15,self.bounds.height*0.07) //x- to move right,y- to move down,W, H
        self.swipingViewNameLabelObject.textColor = UIColor.black
        self.addSubview(swipingViewNameLabelObject)
        
        self.swipingViewLocationLabelObject.text = location
        self.swipingViewLocationLabelObject.font = UIFont(name: "HelveticaNeue-Light", size: 16.0)
        self.swipingViewLocationLabelObject.frame =  CGRect(self.bounds.width/10,self.bounds.height/1.31,self.bounds.width/1.15,self.bounds.height*0.07)//x- to move right,y- to move down,W, H
        self.swipingViewLocationLabelObject.textColor = UIColor.black
        self.addSubview(swipingViewLocationLabelObject)
        
        self.swipingViewOccupationLabelObject.text = occupation
        self.swipingViewOccupationLabelObject.font = UIFont(name: "HelveticaNeue-Light", size: 16.0)
        self.swipingViewOccupationLabelObject.frame = CGRect(self.bounds.width/10,self.bounds.height/1.20,self.bounds.width/1.15,self.bounds.height*0.07) //x- to move right,y- to move down,W, H
        self.swipingViewOccupationLabelObject.textColor = UIColor.black
        self.addSubview(swipingViewOccupationLabelObject)

//        self.applyShadow()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
//    func applyShadow() {
//        self.layer.cornerRadius = 10.0
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOpacity = 0.5
//        self.layer.shadowOffset = CGSize(width: 0.1, height: -3)
//    }
    
    func swipe(answer: Bool) {
        swipingViewAnimator.removeAllBehaviors()
        // If the answer is not like, Move to the left
        // Else if the answer is like, move to the right
        let gravityX = answer ? 0.5 : -0.5
        let magnitude = answer ? 20.0 : -20.0
        let gravityBehavior:UIGravityBehavior = UIGravityBehavior(items: [self])
        gravityBehavior.gravityDirection = CGVector(CGFloat(gravityX), 0)
        swipingViewAnimator.addBehavior(gravityBehavior)
        
        let pushBehavior:UIPushBehavior = UIPushBehavior(items: [self], mode: UIPushBehaviorMode.instantaneous)
        pushBehavior.magnitude = CGFloat(magnitude)
        swipingViewAnimator.addBehavior(pushBehavior)
        
    }
    func returnToCenter() {
        UIView.animate(withDuration: 0.7, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: {
            self.center = self.swipingViewOriginalCenter
        }, completion: { finished in}
        )
    }
}
//// Quick fix for CGReactMake, CGPointMake, CGSizeMake. These are no longer available in Swift 3 therefore in order to use them normally I have added these extensions
extension CGRect
{
    init(_ x:CGFloat,_ y:CGFloat,_ width:CGFloat,_ height:CGFloat)
    {
        self.init(x:x,y:y,width:width,height:height)
    }
    
}
extension CGSize
{
    init(_ width:CGFloat,_ height:CGFloat)
    {
        self.init(width:width,height:height)
    }
}
extension CGPoint
{
    init(_ x:CGFloat,_ y:CGFloat)
    {
        self.init(x:x,y:y)
    }
}
extension CGVector{
    init(_ dx:CGFloat,_ dy:CGFloat) {
        self.init(dx:dx,dy:dy)
    }
}

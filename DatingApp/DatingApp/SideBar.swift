//
//  SideBar.swift
//  Spotter
//
//  Created by Trip Phillips on 8/15/16.
//  Copyright © 2016 JFP Apps. All rights reserved.
//

import UIKit

    @objc protocol SideBarDelegate {
        
        func sideBarDidSelectButtonAtIndex(_ index:Int)
        @objc optional func sideBarWillOpen()
        @objc optional func sideBarWillClose()
        
    }
    
    class SideBar: NSObject, SideBarTableDelegate {
        
        let barWidth: CGFloat = 250
        let sideBarTableTopInset: CGFloat = 64.0
        let sideBarContainerView: UIView = UIView()
        let sideBarTable: SideBarTable = SideBarTable()
        var originalView: UIView?
        
        var animator: UIDynamicAnimator?
        var delegate: SideBarDelegate?
        var isSideBarOpen: Bool = false
        
        override init() {
            
            super.init()
            
        }
        
        init(soureView: UIView, menuItems: Array<String>) {
            
            super.init()
            
            originalView = soureView
            
            sideBarTable.tableData = menuItems
            
            animator = UIDynamicAnimator(referenceView: originalView!)
            
            setupSideBar()
            
            let dragGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector (SideBar.handleDrag(_:)))
            sideBarContainerView.addGestureRecognizer(dragGestureRecognizer)
            
        }
        
        func setupSideBar() {
            
            sideBarContainerView.frame = CGRect(x: (-barWidth - 1), y: (originalView?.frame.origin.y)!, width: barWidth, height: (originalView?.frame.size.height)!)
            sideBarContainerView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
            sideBarContainerView.clipsToBounds = false
            
            originalView?.addSubview(sideBarContainerView)
            
            let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
            blurView.frame = sideBarContainerView.bounds
            sideBarContainerView.addSubview(blurView)
            
            sideBarTable.delegate = self
            sideBarTable.tableView.frame = sideBarContainerView.bounds
            sideBarTable.tableView.clipsToBounds = false
            sideBarTable.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            sideBarTable.tableView.backgroundColor = UIColor.clear
            sideBarTable.tableView.scrollsToTop = false
            sideBarTable.tableView.contentInset = UIEdgeInsetsMake(sideBarTableTopInset, 0, 0, 0)
            
            sideBarTable.tableView.reloadData()
            
            sideBarContainerView.addSubview(sideBarTable.tableView)
            
        }
        
        func handleDrag(_ recognizer: UIPanGestureRecognizer) {
            
            let threshold = sideBarContainerView.bounds.size.width/2 * 0.9
            
            let translation = recognizer.translation(in: self.sideBarContainerView)
            
            recognizer.view!.center = CGPoint(x: recognizer.view!.center.x + translation.x, y: recognizer.view!.center.y)
            
            recognizer.setTranslation(CGPoint.zero, in: self.sideBarContainerView)
            
            if ((recognizer.view?.center.x)! < threshold) {
                
                showSideBar(false)
                delegate?.sideBarWillClose?()
                
            } else {
                
                showSideBar(true)
                delegate?.sideBarWillOpen?()
                
            }
        }
        
        func showSideBar(_ shouldOpen: Bool) {
            
            animator?.removeAllBehaviors()
            isSideBarOpen = shouldOpen
            
            let gravityX: CGFloat = (shouldOpen) ? 12 : -12
            let boundaryX: CGFloat = (shouldOpen) ? barWidth : -barWidth - 1
            
            let gravityBehavior: UIGravityBehavior = UIGravityBehavior(items: [sideBarContainerView])
            gravityBehavior.gravityDirection = CGVector(dx: gravityX, dy: 0)
            animator?.addBehavior(gravityBehavior)
            
            let collisionBehavior: UICollisionBehavior = UICollisionBehavior(items: [sideBarContainerView])
            collisionBehavior.addBoundary(withIdentifier: "sideBarBoundary" as NSCopying, from: CGPoint(x: boundaryX, y: 20), to: CGPoint(x: boundaryX, y: (originalView?.frame.size.height)!))
            animator?.addBehavior(collisionBehavior)
            
            let pushBehavior: UIPushBehavior = UIPushBehavior(items: [sideBarContainerView], mode: UIPushBehaviorMode.instantaneous)
            animator?.addBehavior(pushBehavior)
            
            let sideBarBehavior: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [sideBarContainerView])
            sideBarBehavior.elasticity = 0.0
            sideBarBehavior.resistance = 4.0
            animator?.addBehavior(sideBarBehavior)
            
        }
        
        func sideBarControlDidSelectRow(_ indexPath: IndexPath) {
            
            delegate?.sideBarDidSelectButtonAtIndex(indexPath.row)
            
         }
    }

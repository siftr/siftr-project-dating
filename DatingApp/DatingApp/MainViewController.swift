//
//  MainViewController.swift
//  DatingApp
//
//  Created by Calvin Usiri on 11/15/16.
//  Copyright © 2016 Calvin UsiriAuquem. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import AVKit
import AVFoundation

extension ViewController
{
    func currentViewWasDraggedFunctionHandler(gesture: UIPanGestureRecognizer)
    {
        let currentViewTranslation = gesture.translation(in: self.currentView)
        let currentViewReference = self.currentView
        currentViewReference?.center = CGPoint(x:self.view.bounds.width/2 + currentViewTranslation.x, y: self.view.bounds.height/2 + currentViewTranslation.y)
        let currentViewXCoordinateFromTheCenterOfTheScreen = (currentViewReference?.center.x)! - self.view.bounds.width / 2
        let scaleFactor = min(2000 / abs(currentViewXCoordinateFromTheCenterOfTheScreen), 1) // Determine the size based on how far it is from the center Mins means it cannot be bigger that 1... Bigger Num The smaller the scale
        var currentViewRotationFactor = CGAffineTransform(rotationAngle: currentViewXCoordinateFromTheCenterOfTheScreen / 1450) // Bigger Deno smaller the rotation
        var currentViewStretchFactor = currentViewRotationFactor.scaledBy(x: scaleFactor,y: scaleFactor)
        currentViewReference?.transform = currentViewStretchFactor
        if (gesture.state == UIGestureRecognizerState.ended)
        {
            if (self.currentView.center.x / self.view.bounds.maxX > 0.8)
            {
                self.currentViewAnimationAndOutOfSwipesFunctionHandler(answer: true)
            }
            else if (self.currentView.center.x / self.view.bounds.maxX < 0.2)
            {
                self.currentViewAnimationAndOutOfSwipesFunctionHandler(answer: false)
            }
            else
            {
                self.currentView.returnToCenter()
            }
        }
        currentViewRotationFactor = CGAffineTransform(rotationAngle: 0)
        currentViewStretchFactor = currentViewRotationFactor.scaledBy(x: 1,y: 1)
        currentViewReference?.transform = currentViewStretchFactor
    }
    func handleLogoutFromMainFeed()
    {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        let loginController = LoginSignupViewController()
        present(loginController,animated:true,completion: nil)
        
    } // Whenever logout button is pressed, this function is called and it goes to the loginViewController
    func handleIfUserIsNotLoggedInSegueBackToLoginView()
    {
            print("Not logged in")
            perform(#selector(handleLogoutFromMainFeed), with: nil, afterDelay: 2)
    }
    
    func handleMessage()
    {
        let MessageController = MessageTableViewController()
        let navController = UINavigationController(rootViewController: MessageController)
        present(navController, animated: true, completion: nil)
        print("HandleMessage Function Reached")
    } // When the message icon has been pressed in top right hand corner
    func handleSettings()
    {
        sideBar.showSideBar(true)
    }
    func currentViewAnimationAndOutOfSwipesFunctionHandler(answer: Bool)
    {
        // Run the swipe animation
        self.currentView.swipe(answer: answer)
        self.userTmpArray.removeLast() // O(1) because removes from the last slot without going throught the whole things
        // Handle when we have no more matches
        if self.userTmpArray.count - 1 < 0
        {
            let noMoreView = SwipingView(
                frame: CGRect(self.view.bounds.width/2-185,self.view.bounds.height/2-260,370,500), //x- to move left,y- to move up,W, H
                center: CGPoint(x:self.view.bounds.width/2 , y: self.view.bounds.height/2 ), // This resets the label back to the center of the
                imageTmp: "mark_lewis",
                name: "Out of swipes",
                location: "Out of swipes",
                occupation: "Out of swipes"
            )
            self.userTmpArray.append(noMoreView) //O(1) append
            self.view.addSubview(noMoreView)
            return
        }
        // Set the new current question to the next one
        self.currentView = self.userTmpArray.last!
    }
    func fetchUser()
    {
        FIRDatabase.database().reference().child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            for user in snapshot.children
            {
                let newUser = User(snapshot: user as! FIRDataSnapshot)
                //if you use this setter, your app will crash if your class properties don't exactly match up with the firebase dictionary keys
                self.usersArray.append(newUser) 
                self.currentView = SwipingView(
                    frame: CGRect(self.view.bounds.width/2,self.view.bounds.height/2,self.view.bounds.width*0.95,self.view.bounds.height*0.7), //x- to move left,y- to move up,W relativee, H relative
                    center: CGPoint(x:self.view.bounds.width/2 , y: self.view.bounds.height/2 ), // This resets the label back to the center of the
                    imageTmp: (newUser.userProfileImageUrlStringFromFirebaseObjectReference as String?)!,
                    name: (newUser.userNameStringFromFirebaseObjectReferece as String?)!,
                    location: (newUser.userEmailStringFromFirebaseObjectReference as String?)!,
                    occupation: (newUser.userGenderStringFromFirebaseObjectReference as String?)!
                )
                self.userTmpArray.append(self.currentView) //O(1) append
            }
            for questionView in self.userTmpArray
            {
                self.view.addSubview(questionView)
            }
        }, withCancel: nil)
        // Add Pan Gesture Recognizer
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.currentViewWasDraggedFunctionHandler))
        self.view.addGestureRecognizer(pan)
    }
}

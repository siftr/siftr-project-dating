//
//  SideBarTableView.swift
//  Spotter
//
//  Created by Trip Phillips on 8/15/16.
//  Copyright © 2016 JFP Apps. All rights reserved.
//

import UIKit

    protocol SideBarTableDelegate {
        
        func sideBarControlDidSelectRow(_ indexPath: IndexPath)
        
    }
    
    class SideBarTable: UITableViewController {
        
        var delegate: SideBarTableDelegate?
        var tableData: Array <String> = []
        
        // MARK: - Table view data source
        
        override func numberOfSections(in tableView: UITableView) -> Int {
            
            return 1
            
        }
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return tableData.count
            
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            
            if cell == nil {
                
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
                cell?.backgroundColor = UIColor.clear
                cell?.textLabel?.textColor = UIColor.black
                
                let selectedView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: cell!.frame.size.width, height: cell!.frame.size.height))
                selectedView.backgroundColor = UIColor.clear
                
                cell?.selectedBackgroundView = selectedView
                
            }
            
            cell?.textLabel?.text = tableData[indexPath.row]
            
            return cell!
        }
        
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            self.delegate?.sideBarControlDidSelectRow((indexPath as NSIndexPath) as IndexPath)
            
        }
        
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return 45.0
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }

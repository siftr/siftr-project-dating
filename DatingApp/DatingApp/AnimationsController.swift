//
//  AnimationsControllerMethods.swift
//  DatingApp
//
//  Created by Calvin Usiri on 11/15/16.
//  Copyright © 2016 Calvin UsiriAuquem. All rights reserved.
//

import UIKit

extension LoginSignupViewController {
    
    func loginRegisterAnimations()
    {
        //MARK: ** Animations for loging button **
        UIView.animate(withDuration: 2.0, delay: 0.5, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.loginSignupViewControllerFacebookLoginButton.center.x = self.view.frame.width/2
        }), completion: nil)
        
        //MARK: ** Animations for loging button **
        UIView.animate(withDuration: 2.0, delay: 0.5, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.loginSignupViewControllerTermsAndConditionsButton.center.x = self.view.frame.width/2
        }), completion: nil)
        
        //MARK: ** Animations for loging button **
        UIView.animate(withDuration: 2.0, delay: 0.5, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.loginSignupViewControllerDontWorryLabel.center.x = self.view.frame.width/2
        }), completion: nil)
        

        //MARK: ** Animations for Label Siftr **
        UIView.animate(withDuration: 2.0, delay: 0.5, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.loginSignupViewControllerImagePickerFromLogo.center.x = self.view.frame.width/2
        }), completion: nil)
        
        //MARK: ** Animations for email text field **
        UIView.animate(withDuration: 2.0, delay: 0.2, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.loginSignupViewControllerSegmentedControl.center.x = self.view.frame.width/2
        }), completion: nil)
    }
    // THis handles the animations of when there is an incorrect entry into email and password, buttons move about
    //MARK: ** Animations for Facebook login button **
    func faceBookLoginButtonAnimation()
    {
        self.loginSignupViewControllerFacebookLoginButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.loginSignupViewControllerFacebookLoginButton.transform = .identity
            },
                       completion: nil)
    }

}

extension ViewController
{
    func logoutButtonAnimate(){
    //MARK: ** Animations for signup button **
    UIView.animate(withDuration: 2.0, delay: 0.2, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
    
    self.mainViewControllerLogoutButton.center.x = self.view.frame.width/2
    }), completion: nil)
    }
    
    func navBarButtonsAnimate()
    {
        //MARK: ** Animations for Message button **
        UIView.animate(withDuration: 2.0, delay: 0.1, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.mainViewControllerMessageButton.center.x = self.view.frame.width/2
        }), completion: nil)
        //MARK: ** Animations for Settings button **
        UIView.animate(withDuration: 2.0, delay: 0.3, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.mainViewControllerSettingsButton.center.x = self.view.frame.width/2
        }), completion: nil)
        //MARK: ** Animations for Title Label **
        UIView.animate(withDuration: 2.0, delay: 0.2, usingSpringWithDamping: 1.0, initialSpringVelocity: 10, options: [], animations: ({
            
            self.mainViewControllerTitleLabel.center.x = self.view.frame.width/2
        }), completion: nil)
    }
    
}

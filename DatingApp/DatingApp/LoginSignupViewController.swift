//
//  LoginSignupViewController.swift
//  DatingApp
//
//  Created by Calvin Usiri on 11/15/16.
//  Copyright © 2016 Calvin UsiriAuquem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FBSDKLoginKit
import FBSDKCoreKit
import AVKit
import AVFoundation

class LoginSignupViewController: UIViewController, UINavigationControllerDelegate, FBSDKLoginButtonDelegate,UIImagePickerControllerDelegate
{
    let loginSignupViewControllerFacebookLoginButton = FBSDKLoginButton()
    let loginSignupViewControllerDontWorryLabel = UILabel()
    let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    let loginSignupViewControllerTermsAndConditionsButton = UIButton()
    let loginSignupViewControllerSegmentedControl = UISegmentedControl(items: ["Males", "Females"])
    var isInterestedIn = "Female"
    let loginSignupViewControllerImagePickerFromLogo = UIImageView()
    var ref: FIRDatabaseReference!
    var user : [String: Any]? = nil
    //MARK: ** Video players **
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        view.backgroundColor = UIColor(white:1,alpha : 1) // gives the grey color
        let x = self.view.frame.width
        let y = self.view.frame.height
        let center = x/2
        
        let theURL = NSURL(fileURLWithPath: "/Users/calvinusiri/Desktop/backGroundApp.mp4")
        
        avPlayer = AVPlayer(url: theURL as URL)
        // Invoke after player is created and AVPlayerItem is specified
        NotificationCenter.default.addObserver(self,selector:  #selector(LoginSignupViewController.playerItemDidReachEnd(notification:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: self.avPlayer.currentItem)
        avPlayer = AVPlayer.init(url: theURL as URL)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        avPlayer.volume = 0
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        
        avPlayerLayer.frame = view.layer.bounds
        view.backgroundColor = UIColor.clear;
        view.layer.insertSublayer(avPlayerLayer, at: 0)
        
        loginSignupViewControllerImagePickerFromLogo.center.x = x + 30
        loginRegisterAnimations() // Animations method is in LoginRegisterAnimationsa
        
        //MARK: ** Siftr Title Label **
        loginSignupViewControllerImagePickerFromLogo.frame = CGRect(x: self.view.bounds.width/2,y: self.view.bounds.height/2,width: 300,height: 300)
        loginSignupViewControllerImagePickerFromLogo.center = CGPoint(x: center, y: y/4)
        loginSignupViewControllerImagePickerFromLogo.tintColor = UIColor.white
        loginSignupViewControllerImagePickerFromLogo.image = UIImage(named: "siftrapp")
        loginSignupViewControllerImagePickerFromLogo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTappedTitle)))
        loginSignupViewControllerImagePickerFromLogo.isUserInteractionEnabled = true //default to false
        self.view.addSubview(loginSignupViewControllerImagePickerFromLogo)
        
        loginSignupViewControllerSegmentedControl.frame = CGRect(x: self.view.bounds.width/2, y: self.view.bounds.height/2, width: x*0.9, height: 50)
        loginSignupViewControllerSegmentedControl.center = CGPoint(x: center, y: y/1.5)
        loginSignupViewControllerSegmentedControl.tintColor = UIColor.white
        //        segControl.selectedSegmentIndex = 1 // Preselected toggle
        self.view.addSubview(loginSignupViewControllerSegmentedControl)
        
        //MARK: ** Facebook Login Button **
        loginSignupViewControllerFacebookLoginButton.delegate = self
        loginSignupViewControllerFacebookLoginButton.backgroundColor = UIColor.clear
        loginSignupViewControllerFacebookLoginButton.readPermissions = ["email", "public_profile"]
        loginSignupViewControllerFacebookLoginButton.frame = CGRect(x: self.view.bounds.width/2, y: self.view.bounds.height/2, width: x*0.9, height: 50)//x- to move left,y- to move up,W, H
        loginSignupViewControllerFacebookLoginButton.center = CGPoint(x: center, y: y/1.3)
        loginSignupViewControllerFacebookLoginButton.layer.masksToBounds = true
        loginSignupViewControllerFacebookLoginButton.layer.cornerRadius = 20 //curvy edges
        self.view.addSubview(loginSignupViewControllerFacebookLoginButton)
        
        loginSignupViewControllerDontWorryLabel.frame = CGRect(x: self.view.bounds.width/2, y: self.view.bounds.height/2, width: x/2.1, height: 50)//x- to move left,y- to move up,W,H
        loginSignupViewControllerDontWorryLabel.center = CGPoint(x: center, y: y/1.19)
        loginSignupViewControllerDontWorryLabel.text = "We never post on Facebook!"
        loginSignupViewControllerDontWorryLabel.textAlignment = NSTextAlignment(rawValue: Int(center))!
        loginSignupViewControllerDontWorryLabel.font = UIFont(name: "HelveticaNeue-Light", size: 15.0)
        loginSignupViewControllerDontWorryLabel.textColor = UIColor.white
        self.view.addSubview(loginSignupViewControllerDontWorryLabel)
        
        loginSignupViewControllerTermsAndConditionsButton.frame = CGRect(x: self.view.bounds.width/2, y: self.view.bounds.height/2, width: x/2.1, height: 50)//x- to move left,y- to move up,W, H
        loginSignupViewControllerTermsAndConditionsButton.center = CGPoint(x: center, y: y/1.1)
        loginSignupViewControllerTermsAndConditionsButton.setTitle("Terms and Conditions", for: UIControlState())
        loginSignupViewControllerTermsAndConditionsButton.setTitleColor(UIColor.white, for: UIControlState())
        loginSignupViewControllerTermsAndConditionsButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 15.0)
        loginSignupViewControllerTermsAndConditionsButton.addTarget(self, action: #selector(handleTermsAndConditions), for: .touchUpInside)
        self.view.addSubview(loginSignupViewControllerTermsAndConditionsButton)
    }
    
    func handleTappedTitle()
    {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true // Allows you to edit the picture within the phone
        print("Picker Clicked working")
        present(picker, animated: true , completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("Cancel picker")
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var selectedImageFromPicker :UIImage?
        
        if let editedImage = info["UIPickerControllerEditedImage"] as? UIImage // After cropping, you want to keep the editted image
        {
            print(editedImage.size)
            selectedImageFromPicker = editedImage
        }
        else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage
        {
                selectedImageFromPicker = originalImage
        }
        if let selectedImage = selectedImageFromPicker
        {
            loginSignupViewControllerImagePickerFromLogo.image = selectedImage
        }
            dismiss(animated: true, completion: nil)
    }
       //MARK: Keyboard Returns On Return Button
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        self.view.endEditing(true)
        return false
        
    }
    
    //MARK: Keyboard Returns On Screen Touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        avPlayer.play()
        paused = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        avPlayer.pause()
        paused = true
    }
    func playerItemDidReachEnd(notification: NSNotification) {
        self.avPlayer.seek(to: kCMTimeZero)
        self.avPlayer.play()
        
    }

}


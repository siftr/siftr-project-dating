////
////  User.swift
////  DatingApp
////
////  Created by Calvin Usiri on 12/25/16.
////  Copyright © 2016 Calvin UsiriAuquem. All rights reserved.
////
//
import UIKit
import Firebase
import FirebaseDatabase

class User 
{
    var userNameStringFromFirebaseObjectReferece: String? // Created an object that holds references to these things within snapshot
    var userGenderStringFromFirebaseObjectReference : String?
    var userProfileImageUrlStringFromFirebaseObjectReference : String?
    var userIsInterestedInStringFromFirebaseObjectReference : String?
    var userEmailStringFromFirebaseObjectReference : String?
    var refStruct : FIRDatabaseReference?
    
    
    init(snapshot: FIRDataSnapshot)
    {
        let dictionary = snapshot.value as! NSDictionary
        print("Testing dictionary from Struct", dictionary)
        refStruct = snapshot.ref
        userNameStringFromFirebaseObjectReferece = dictionary["name"] as! String?
        userEmailStringFromFirebaseObjectReference = dictionary["email"] as! String?
        userProfileImageUrlStringFromFirebaseObjectReference = dictionary["profileURL"] as! String?
        userIsInterestedInStringFromFirebaseObjectReference = dictionary["isInterestedIn"] as! String?
        userGenderStringFromFirebaseObjectReference = dictionary["gender"] as! String?
        
    }
    init()
    {
        let userNameFromFirebaseAsString = userNameStringFromFirebaseObjectReferece as String?
        let userGenderFromFirebaseAsString = userGenderStringFromFirebaseObjectReference as String?
        let userProfileImageUrlFromFirebaseAsString = userProfileImageUrlStringFromFirebaseObjectReference as String?
        let userIsInterestedInFromFirebaseAsString = userIsInterestedInStringFromFirebaseObjectReference as String?
        let userEmailFromFirebaseAsString = userEmailStringFromFirebaseObjectReference as String?
        print("Testing naming", userNameFromFirebaseAsString as Any)
        print("Testing gendering", userGenderFromFirebaseAsString as Any)
        print("Testing profileing", userProfileImageUrlFromFirebaseAsString as Any)
        print("Testing interesteding", userIsInterestedInFromFirebaseAsString as Any)
        print("Testing emailing", userEmailFromFirebaseAsString as Any)
        
    }
}

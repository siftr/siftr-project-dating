//
//  SideBarView.swift
//  Spotter
//
//  Created by Trip Phillips on 8/15/16.
//  Copyright © 2016 JFP Apps. All rights reserved.
//

import UIKit

    @objc protocol SideBarViewDelegate {
        
        @objc optional func sideBarWillOpen()
        @objc optional func sideBarWillClose()
        
    }
    
    class SideBarView: NSObject {
        
        let viewHeight: CGFloat = 0
        let sideBarViewContainer: UIView = UIView()
        var originalView: UIView?
        
        var animator: UIDynamicAnimator?
        var delegate: SideBarViewDelegate?
        var issideBarViewOpen: Bool = false
        
        var lastLocation:CGFloat = CGFloat()
     
        override init() {
            
            super.init()
            
        }
        
        init(soureView: UIView) {
            
            super.init()
            
            originalView = soureView
            
            animator = UIDynamicAnimator(referenceView: originalView!)
            
            setupsideBarView()
            
            let dragGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector (SideBarView.handleDrag(_:)))
            sideBarViewContainer.addGestureRecognizer(dragGestureRecognizer)
            
        }
        
        func setupsideBarView() {
            
            let width = originalView?.frame.size.width
            let height = originalView?.frame.size.height
            
            sideBarViewContainer.frame = CGRect(x: 0 , y: 0, width: width! / 30 * 30, height: height! / 30 * 30)
            sideBarViewContainer.center = CGPoint(x: width!/2, y: height! * 1.5)
            sideBarViewContainer.backgroundColor = UIColor.white
            sideBarViewContainer.clipsToBounds = false
            
            originalView?.addSubview(sideBarViewContainer)
            
            let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
            blurView.frame = sideBarViewContainer.bounds
            sideBarViewContainer.addSubview(blurView)
            
            let exitButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            exitButton.center = CGPoint(x: sideBarViewContainer.frame.width/20 * 1.5, y: sideBarViewContainer.frame.height/20 * 1.0)
            exitButton.backgroundColor = UIColor.black
            exitButton.addTarget(self, action: #selector(SideBarView.exitAction(_:)), for: UIControlEvents.touchUpInside)
            sideBarViewContainer.addSubview(exitButton)
            
            
        }
        
        func handleDrag(_ recognizer: UIPanGestureRecognizer) {
            
            let threshold = sideBarViewContainer.bounds.size.height/2 * 1.3
            
            let translation = recognizer.translation(in: self.sideBarViewContainer)
            
            recognizer.view!.center = CGPoint(x: recognizer.view!.center.x, y: recognizer.view!.center.y + translation.y)
            
            recognizer.setTranslation(CGPoint.zero, in: self.sideBarViewContainer)
            
            if ((recognizer.view?.center.y)! > threshold) {
                
                showSideBarView(false)
                delegate?.sideBarWillClose?()
                
            } else {
                
                showSideBarView(true)
                delegate?.sideBarWillOpen?()
                
            }
        }
        
        func showSideBarView(_ shouldOpen: Bool) {
            
            animator?.removeAllBehaviors()
            issideBarViewOpen = shouldOpen
            let height = originalView?.frame.size.height
            
            let gravityY: CGFloat = (shouldOpen) ? -12 : 12
            let boundaryY: CGFloat = (shouldOpen) ? viewHeight : (height! * 2.05 )
            
            let gravityBehavior: UIGravityBehavior = UIGravityBehavior(items: [sideBarViewContainer])
            gravityBehavior.gravityDirection = CGVector(dx: 0, dy: gravityY)
            animator?.addBehavior(gravityBehavior)
            
            let collisionBehavior: UICollisionBehavior = UICollisionBehavior(items: [sideBarViewContainer])
            collisionBehavior.addBoundary(withIdentifier: "sideBarViewBoundary" as NSCopying, from: CGPoint(x: 0, y: boundaryY), to: CGPoint(x: (originalView?.frame.size.width)!, y: boundaryY))
            animator?.addBehavior(collisionBehavior)
            
            let pushBehavior: UIPushBehavior = UIPushBehavior(items: [sideBarViewContainer], mode: UIPushBehaviorMode.instantaneous)
            pushBehavior.pushDirection = CGVector(dx: 0, dy: viewHeight)
            animator?.addBehavior(pushBehavior)
            
            let sideBarViewBehavior: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [sideBarViewContainer])
            sideBarViewBehavior.elasticity = 0.0
            sideBarViewBehavior.resistance = 4.0
            animator?.addBehavior(sideBarViewBehavior)
            
            originalView?.bringSubview(toFront: sideBarViewContainer)
            
        }
        
        func exitAction(_ sender: UIButton) {
            
            showSideBarView(false)
            delegate?.sideBarWillClose?()
            
        }
    }

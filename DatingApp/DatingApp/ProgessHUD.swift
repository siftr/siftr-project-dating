//
//  ActivityIndicator.swift
//  DatingApp
//
//  Created by Calvin Usiri on 1/4/17.
//  Copyright © 2017 Calvin UsiriAuquem. All rights reserved.
//

import UIKit

class ProgressHUD: UIVisualEffectView
{
    //Head up display
    var text: String?
    {
        didSet
        {
            label.text = text
        }
    }
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    let label: UILabel = UILabel()
    let blurEffect = UIBlurEffect(style: .light)
    let vibrancyView: UIVisualEffectView
    
    init(text: String)
    {
        self.text = text
        self.vibrancyView = UIVisualEffectView(effect: blurEffect)
        super.init(effect: blurEffect)
        self.setup()
    }
    required init(coder aDecoder: NSCoder)
    {
        self.text = ""
        self.vibrancyView = UIVisualEffectView(effect:  blurEffect)
        super.init(coder: aDecoder)!
        self.setup()
        
    }
    func setup()
    {
        contentView.addSubview(vibrancyView)
        vibrancyView.contentView.addSubview(activityIndictor)
        vibrancyView.contentView.addSubview(label)
        activityIndictor.startAnimating()
    }
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let superview = self.superview
        {
            let width = superview.frame.size.width*2/3
            let height: CGFloat = 50.0
            self.frame = CGRect(superview.frame.size.width / 2 - width / 2,superview.frame.height / 2 - height / 2,width,height)
            vibrancyView.frame = self.bounds
            let activityIndicatorSize: CGFloat = 50
            activityIndictor.frame = CGRect(5, height / 2 - activityIndicatorSize / 2,activityIndicatorSize,activityIndicatorSize)
            layer.cornerRadius = 8.0
            layer.masksToBounds = true
            label.text = text
            label.textAlignment = NSTextAlignment.center
            label.frame = CGRect(activityIndicatorSize + 5, 0, width - activityIndicatorSize - 15, height)
            label.textColor = UIColor.black
            label.font = UIFont(name: "HelveticaNeue-Light", size: 16.0)
        }
    }
    func show() {
        self.isHidden = false
    }
    func hide() {
        self.isHidden = true
    }
}

//
//  loginRegisterControllerMethods.swift
//  DatingApp
//
//  Created by Calvin Usiri on 11/15/16.
//  Copyright © 2016 Calvin UsiriAuquem. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import AVKit
import AVFoundation

extension LoginSignupViewController
{

    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)
    {
        
        UserDefaults.standard.set(false, forKey: "isLoggedIn") 
        print("The User logged out using FB logout")
        
    }
    func handleLogout()
    {
        do
        {
            try FIRAuth.auth()?.signOut()
        } catch let logoutError
        {
            print(logoutError)
        }
        
        let loginController = LoginSignupViewController()
        present(loginController,animated:true,completion: nil)
        
    } //This function is called and it goes to the loginViewController
    
    //MARK: Facebook Login Button Action
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        faceBookLoginButtonAnimation()
        if error != nil {
            print(error)
            
            return
        }

        fbGraph()
    }
    
    func fbGraph()
    {
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else { return }
        
        let credentials = FIRFacebookAuthProvider.credential(withAccessToken: accessTokenString)
        FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
            if error != nil {
                print("Something went wrong with our FB user: ", error ?? "")
                return
            }
            
            print("Successfully logged in with our user: ", user ?? "")
        })
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, gender, about, languages, religion, birthday "]).start { (connection, result, err) in
            
            
            if err != nil
            {
                print("Failed to start graph request:", err ?? "")
                return
            }
            print(result ?? "")
            
            self.user = result as? [String: Any]
            
            //Firebase storage to upload an Image to firebase
            let storageRef = FIRStorage.storage().reference().child("profilePictures").child(" \(self.user?["id"]) UserImage")
            if let uploadData = UIImagePNGRepresentation(self.loginSignupViewControllerImagePickerFromLogo.image!)
            {
                storageRef.put(uploadData, metadata: nil, completion: { (metaData, error) in
                    if(error != nil)
                    {
                        print(error!)
                        return
                    }
                    if let profileImageUrl = metaData?.downloadURL()?.absoluteString
                    {
                        if(self.loginSignupViewControllerSegmentedControl.selectedSegmentIndex == 0)
                        {
                            //Value 0 = Male
                            self.isInterestedIn = "male"
                        }
                        let values = ["name": self.user?["name"] as! String, "email": self.user?["email"] as! String,"gender": self.user?["gender"] as! String, "isInterestedIn":self.isInterestedIn, "profileURL": profileImageUrl]
                        self.handleFireBaseLoginValues(values: values as [String : AnyObject])
                    }
                    print(metaData!)
                })
            }
            //saving data into database
            UserDefaults.standard.set(true, forKey: "isLoggedIn")
            print("is user logged in \(UserDefaults.standard.bool(forKey: "isLoggedIn"))")
            self.activityIndicator.startAnimating()
            let viewController = ViewController()
            self.present(viewController, animated: true, completion: nil) // presents the seque to MainViewController
        }
    }
    func handleTermsAndConditions()
    {
        print("Segue to the Terms and Conditions View")
    }
    private func handleFireBaseLoginValues(values: [String:AnyObject])
    {
        let ref = FIRDatabase.database().reference(fromURL: "https://siftrdatingapp.firebaseio.com/") // allocate ref
        let usersReference = ref.child("users").child(user?["id"] as! String)//child reference, uid is FB userId
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            if err != nil
            {print(err!)
                return}
        })
    }
}

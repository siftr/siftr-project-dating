//
//  MainViewController.swift
//  DatingApp
//
//  Created by Calvin Usiri on 11/15/16.
//  Copyright © 2016 Calvin UsiriAuquem. All rights reserved.
//

import UIKit
import Firebase
class ViewController: UIViewController, SideBarDelegate, SideBarViewDelegate, SideBarTableDelegate
{
    func sideBarDidSelectButtonAtIndex(_ index: Int)
    {
        let messageController = MessageTableViewController()
        present(messageController, animated: true, completion: nil)
    }
    func sideBarControlDidSelectRow(_ indexPath: IndexPath)
    {
        let messageController = MessageTableViewController()
        present(messageController, animated: true, completion: nil)
    }
    let mainViewControllerLogoutButton = UIButton()
    let swipingView = UIView()
    let mainViewControllerMessageButton = UIButton(type: .custom)
    let mainViewControllerSettingsButton = UIButton(type: .custom)
    let mainViewControllerTitleLabel = UILabel()
    var sideBar = SideBar()
    var sideBarView = SideBarView()
    var isInterestedITmp :String?
    let logBool = UserDefaults.standard.bool(forKey: "isLoggedIn")
    //    let userTmp = FIRAuth.auth()?.currentUser
    
    var usersArray = [User]() // Creating a stack essentially but array based
    var userTmpArray = [SwipingView]()
    var currentView: SwipingView!
    var rover : Int?
    
    override func viewDidLoad()
    {
        let center = view.frame.width / 2
        let x = view.frame.width
        let y = view.frame.height
        
        view.backgroundColor = UIColor(white:1,alpha : 1) // gives the grey color
        
        fetchUser() // To get users from firebase and add them to the stack
        logoutButtonAnimate()
        navBarButtonsAnimate()
        
        // Side bar set up
        sideBarView = SideBarView(soureView: self.view)
        sideBarView.delegate = self
        sideBar = SideBar(soureView: self.view, menuItems: (["Test Match"]))
    
        
        // Navbar set up
        let topBar = UIView(frame: CGRect(0,0,x,y/10))
        topBar.backgroundColor = UIColor(white:0.99,alpha:1)
        self.view.addSubview(topBar)
        
        // NavBar message button
        if let mainViewNavigationBarMessageButton = UIImage(named: "message_icon")
        {
            mainViewControllerMessageButton.setImage(mainViewNavigationBarMessageButton, for: .normal)
            mainViewControllerMessageButton.frame = CGRect(x: x/2+155, y: 30, width: 35, height: 30) // smaller x move to left, smaller y move up
            mainViewControllerMessageButton.addTarget(self, action: #selector(handleMessage), for: .touchUpInside)
            self.view.addSubview(mainViewControllerMessageButton)
        }
        
        // Siftr Title on MainViewController
        mainViewControllerTitleLabel.text = "Siftr"
        mainViewControllerTitleLabel.textColor = UIColor.blue
        mainViewControllerTitleLabel.frame = CGRect(x/2-100,-4,200,y/10+30)
        mainViewControllerTitleLabel.textAlignment = NSTextAlignment.center
        mainViewControllerTitleLabel.font = UIFont(name: "HelveticaNeue-light", size: 35.0)
        self.view.addSubview(mainViewControllerTitleLabel)
        
        // NavBar Settings button
        if let image = UIImage(named: "settings_icon")
        {
            mainViewControllerSettingsButton.setImage(image, for: .normal)
            mainViewControllerSettingsButton.frame = CGRect(x/2-190,30,35,30) // smaller x move to left, smaller y move up
            mainViewControllerSettingsButton.addTarget(self, action: #selector(handleSettings), for: .touchUpInside)
            self.view.addSubview(mainViewControllerSettingsButton)
        }
        mainViewControllerLogoutButton.frame = CGRect(x: 100, y: 100, width: 150, height: 100)
        mainViewControllerLogoutButton.backgroundColor = UIColor.clear
        mainViewControllerLogoutButton.setTitle("Logout", for: UIControlState())
        mainViewControllerLogoutButton.setTitleColor(UIColor.blue, for: UIControlState())
        mainViewControllerLogoutButton.titleLabel?.font = UIFont(name: "HelveticaNeue-UltraLight", size: 35.0)
        mainViewControllerLogoutButton.center = CGPoint(x: center, y: y/20 * 18)
        mainViewControllerLogoutButton.addTarget(self, action: #selector(handleLogoutFromMainFeed), for: .touchUpInside)
        self.view.addSubview(mainViewControllerLogoutButton)
    
        sideBar.setupSideBar()
        sideBarView.setupsideBarView()
    
        //Label and SwipingImage
        if(logBool == false)
        {
            handleIfUserIsNotLoggedInSegueBackToLoginView()
        }

    }
}
